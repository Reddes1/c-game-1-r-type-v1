#include <assert.h>
#include <string>
#include <iostream>
#include <sstream>

#include <iomanip>      // std::setprecision

#include "SFML/Graphics.hpp"

// SFML namespace
using namespace sf;

using namespace std;

//dimensions in 2D that are whole numbers
struct Dim2Di
{
    int x, y;
};

//dimensions in 2D that are floating point numbers
struct Dim2Df
{
    float x, y;
};

/*
A box to put Games Constants in.
These are special numbers with important meanings (screen width,
ascii code for the escape key, number of lives a player starts with,
the name of the title screen music track, etc.
*/
namespace GC
{
    //game play related constants to tweak
    const Dim2Di SCREEN_RES{ 1024, 800 };

    const char ESCAPE_KEY{ 27 };
}

// Texture Load checker. 
bool LoadTexture(const string& file, Texture& tex)
{
    if (tex.loadFromFile(file))
    {
        tex.setSmooth(false);
        return true;
    }
    assert(false);
    return false;
}

// Core for all the game related variables and functions.
struct GameCore {

    //Player Sprites
    Texture shipTex;
    Sprite playerSpr;

    Texture shotTex1;
    Sprite shotSpr1;

    //Terrain Sprites
    Texture bgndTex1;
    Sprite bgndSpr1;

    Texture fgndTex1;
    Sprite fgndSpr1;

    //Fonts
    Font font;

    //Terrain Scroll Values & Containers
    const float terrainScrollSpd1 = 18; // Background
    const float terrainScrollSpd2 = 25.6; // Foreground
    float terrainScroll1 = 0.f;
    float terrainScroll2 = 0.f;

    // Vectors
    vector<Sprite> playerProjectiles;
    vector<Sprite> enemyProjectiles;
    vector<Sprite> enemies;
    vector<int> xCord;
    vector<int> yCord;

    //Player Values
    const float playerSpeed = 300.0f;
    const float projectileSpeed = 360.0f;
    float shotDelay = 30;
    const float shotDelayCap = 20;
    const float playerShotOffset = 18;

    //UI Values
    float score = 0;
    float playerLives = 3;

    //FUNCTION CALLS
    // Update / User Control
    void Update(RenderWindow& window, float elapsed);

    // Pre-Load Manager
    void PreGameSetup(RenderWindow& window);

    // Asset Loading Block
    void LoadTerrainAssets();
    void LoadPlayerAssets(RenderWindow& window);
    void LoadEnemyAssets(RenderWindow& window);
    void LoadUIAssets(RenderWindow& window);

    // Render Block
    void RenderManager(RenderWindow& window, float elapsed, float elapsedGlobalTime);
    void TerrainRender(RenderWindow& window, float elasped, float elapsedGlobalTime);
    void UIRender(RenderWindow& window, float elapsed, float elapsedGlobalTime);
    void PlayerRender(RenderWindow& window, float elasped);
    void EnemyRender(RenderWindow& window, float elapsed, float elapsedGlobalTime);
    void ProjectileRender(RenderWindow& window, float elapsed);
};

struct TerrainCollisions
{

};

int main()
{
    // Create the main window
    RenderWindow window(VideoMode(GC::SCREEN_RES.x, GC::SCREEN_RES.y), "PROJECT: Valkyrie");
    // Internal Clock
    Clock clock;
    Clock timer;

    // Core Game Object
    GameCore game;

    // Call manager for setting up game
    game.PreGameSetup(window);

    // Start game loop 
    while (window.isOpen())
    {
        // Setting up FPS Syncing
        float elapsedTime = clock.getElapsedTime().asSeconds();
        float elapsedGlobalTime = timer.getElapsedTime().asSeconds();

        clock.restart();

        //Timer Restart Test
        if (Keyboard::isKeyPressed(Keyboard::K))
        {
            timer.restart();
        }

        //Call update
        game.Update(window, elapsedTime);

        // Clear previous frame
        window.clear();

        // Call new frame render
        game.RenderManager(window, elapsedTime, elapsedGlobalTime);

        // Display new frame
        window.display();
        // Process events
        Event event;
        while (window.pollEvent(event))
        {
            // Close window: exit
            if (event.type == Event::Closed)
                window.close();
            if (event.type == Event::TextEntered)
            {
                if (event.text.unicode == GC::ESCAPE_KEY)
                    window.close();
            }
        }


    }

    return EXIT_SUCCESS;
}

// Manager for all pre-load related functions.
void GameCore::PreGameSetup(RenderWindow& window)
{
    LoadTerrainAssets();
    LoadPlayerAssets(window);
    LoadEnemyAssets(window);
    LoadUIAssets(window);
}

// Manages loading Terrain related sprites.
void GameCore::LoadTerrainAssets()
{
    // Background Assets
    LoadTexture("data/background/background1.png", bgndTex1);
    bgndSpr1.setTexture(bgndTex1);
    bgndTex1.setRepeated(true);

    // Foreground Assets
    LoadTexture("data/background/foreground2.png", fgndTex1);
    fgndSpr1.setTexture(fgndTex1);
    fgndTex1.setRepeated(false);
    fgndTex1.setSmooth(true);
    fgndSpr1.setScale(1.92f, 1.92f);

    if (!font.loadFromFile("data/fonts/comic.ttf"))
        assert(false);


}

// Manages loading Player related sprites. 
void GameCore::LoadPlayerAssets(RenderWindow& window)
{
    // Alternate load method
    /* if (!LoadTexture("data/player/ship.png", shipTex))
     assert(false);*/

     // Player Assets
    LoadTexture("data/player/ship.png", shipTex);

    // Position Values
    float x = window.getSize().x / 7.f;
    float y = window.getSize().y / 2.f;

    // Initial Player Sprite Creation
    IntRect playerShipR = playerSpr.getTextureRect();
    playerSpr.setOrigin(playerShipR.width / 2.f, playerShipR.height / 2.f);
    playerSpr.setTexture(shipTex);
    playerSpr.setPosition(x, y);

    //Projectile Sprite
    LoadTexture("data/player/shot1.png", shotTex1);
    shotSpr1.setTexture(shotTex1);
    shotSpr1.setScale(2.f, 2.f);
}

// Manages loading Enemy related sprites.
void GameCore::LoadEnemyAssets(RenderWindow& window)
{

}

void GameCore::LoadUIAssets(RenderWindow& window)
{

}

// Governs player movement and shooting based on player inputs as well as keeping the player in
// bounds of the window.
void GameCore::Update(RenderWindow& window, float elapsed)
{

    // Player Boundary setting variables (to limit movement further within the screen)
    float boundaryY = 10.0f;
    float boundaryX = 15.0f;

    // Grab the current player sprite position at the start of the frame
    Vector2f pos = playerSpr.getPosition();

    // Player Movement Code
    if (Keyboard::isKeyPressed(Keyboard::W))
    {
        pos.y -= playerSpeed * elapsed;
    }
    if (Keyboard::isKeyPressed(Keyboard::A))
    {
        pos.x -= playerSpeed * elapsed;
    }
    if (Keyboard::isKeyPressed(Keyboard::S))
    {
        pos.y += playerSpeed * elapsed;
    }
    if (Keyboard::isKeyPressed(Keyboard::D))
    {
        pos.x += playerSpeed * elapsed;
    }

    // Contain the player to the window by checking for if they moved the sprite out of bounds,
// and resetting the the position at the start of the frame if they did.
    if ((pos.y > playerSpr.getGlobalBounds().height / 2.0f + boundaryY) &&
        (pos.x > playerSpr.getGlobalBounds().width / 2.0f + boundaryX) &&
        (pos.y < GC::SCREEN_RES.y - boundaryY - playerSpr.getGlobalBounds().height / 2.0f) &&
        (pos.x < GC::SCREEN_RES.x - playerSpr.getGlobalBounds().width / 2.0f))
    {
        playerSpr.setPosition(pos);
    }

    // Shooting Updates

    // Shot Delay Counter
    if (shotDelay < shotDelayCap)
    {
        shotDelay++;
    }

    // Fire Shot
    if (Keyboard::isKeyPressed(Keyboard::J) && shotDelay >= shotDelayCap)
    {
        shotSpr1.setPosition(Vector2f(playerSpr.getPosition().x + playerShotOffset, playerSpr.getPosition().y));
        playerProjectiles.push_back(Sprite(shotSpr1));
        score = score + 10;

        shotDelay = 0;

    }

    // Shot Speed & Off-screen Erasure
    for (size_t i = 0; i < playerProjectiles.size(); i++)
    {
        playerProjectiles[i].move(projectileSpeed * elapsed, 0);

        if (playerProjectiles[i].getPosition().x >= window.getSize().x)
        {
            playerProjectiles.erase(playerProjectiles.begin() + i);
        }
    }
}

// Manager for pointing to each render fuction.
void GameCore::RenderManager(RenderWindow& window, float elapsed, float elapsedGlobalTime)
{
    TerrainRender(window, elapsed, elapsedGlobalTime);
    UIRender(window, elapsed, elapsedGlobalTime);
    PlayerRender(window, elapsed);
    EnemyRender(window, elapsed, elapsedGlobalTime);
    ProjectileRender(window, elapsed);
}

// Holds all the code for rendering the level terrain for each frame.
void GameCore::TerrainRender(RenderWindow& window, float elapsed, float elapsedGlobalTime)
{
    //Background 1

    IntRect bgndRect1((float)terrainScroll1, 0, window.getSize().x, window.getSize().y); // (Left, Top, X Size, Y Size)
    terrainScroll1 += terrainScrollSpd1 * elapsed;
    bgndSpr1.setTextureRect(bgndRect1);
    window.draw(bgndSpr1);

    //Foreground 1
    IntRect fgndRect1((float)terrainScroll2, 0, window.getSize().x, window.getSize().y);
    terrainScroll2 += terrainScrollSpd2 * elapsed;
    fgndSpr1.setTextureRect(fgndRect1);
    window.draw(fgndSpr1);
}

void GameCore::UIRender(RenderWindow& window, float elapsed, float elapsedGlobalTime)
{

    Text playerScoreDisplay("Score: ", font, 24);
    playerScoreDisplay.setPosition(2, 1);
    window.draw(playerScoreDisplay);

    Text playerScore;
    Int32 myInt = score;
    playerScore.setString(to_string(myInt));

    playerScore.setFont(font);
    playerScore.setCharacterSize(24);
    playerScore.setPosition(80, 2);
    window.draw(playerScore);

    cout << setprecision(5) << static_cast<int>(elapsedGlobalTime) << "\n";
    //cout << window.getSize().x << "\n"; 
    //cout << window.getSize().y << "\n";
}

// Holds all the code for rendering the player sprite.
void GameCore::PlayerRender(RenderWindow& window, float elapsed)
{
    //Ship
    IntRect texR = playerSpr.getTextureRect();
    playerSpr.setOrigin(shipTex.getSize().x / 2.f, shipTex.getSize().y / 2.f);
    playerSpr.setScale(2.0f, 2.0f);
    window.draw(playerSpr);
}

void GameCore::EnemyRender(RenderWindow& window, float elapsed, float elapsedGlobalTime)
{
    if (elapsedGlobalTime > 5.0f)
    {

    }
}

void GameCore::ProjectileRender(RenderWindow& window, float elapsed)
{
    for (size_t i = 0; i < playerProjectiles.size(); i++)
    {
        window.draw(playerProjectiles[i]);
    }
}